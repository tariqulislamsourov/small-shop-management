from django.urls import path
from django.urls.resolvers import URLPattern


from products.views import searchProduct
app_name = 'products'

urlpatterns = [
    path('search-product/', searchProduct, name='search_product')
]