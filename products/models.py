from typing import Tuple
from django.db import models
from django.db.models.expressions import F
from shop.models import ShopDetail
# Create your models here.

class Brand(models.Model):
    class Meta:
        db_table = 'brands'

    def __str__(self):
        return self.brand_name
    
    brand_name = models.CharField(max_length= 30, null=False, default='')


class ProductType(models.Model):
    class Meta:
        db_table = 'product_type'

    def __str__(self):
        return self.type_name
    
    type_name = models.CharField(max_length= 30, null=False, default='')


class Products(models.Model):
    class Meta:
        db_table = 'products'

    def __int__(self):
        return self.id
    
    p_type = models.ForeignKey(ProductType, null=True, default=0, on_delete = models.SET_NULL)
    brand = models.ForeignKey(Brand, null=True, default=0, on_delete = models.SET_NULL)
    name = models.CharField(max_length= 30, null=False, default='')
    description = models.TextField(null=True, default='')
    p_image = models.ImageField(upload_to = 'image/products')
    volume_weight = models.CharField(max_length= 20, null=False, default='')
    buying_price = models.IntegerField(null=False, default=0)
    available_quantity = models.IntegerField(null=False, default=0)
    bar_code = models.CharField(max_length= 30, null=False, default='')
    bar_code_image = models.ImageField(upload_to = 'image/bar_codes')
    selling_price = models.IntegerField(null=False, default=0)
    shop = models.ForeignKey(ShopDetail, on_delete= models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)

from shop.models import ShopDetail

class SellDetails(models.Model):
    class Meta:
        db_table = 'sell_detail'


    product = models.ForeignKey(Products, null=False, on_delete= models.CASCADE)
    shop = models.ForeignKey(ShopDetail, null=False, on_delete=models.CASCADE)
    quantity = models.IntegerField(null=False)
    selling_price = models.IntegerField(null=False)
    profit = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    