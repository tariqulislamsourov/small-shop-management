from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.core.files.storage import FileSystemStorage
from .models import Brand, ProductType, Products
from .forms import ProductForm, BrandForm, ProductTypeForm
from django.db.models import Q
from xlrd import open_workbook
import os

from account.views import insert_data_in_userType

def home(request):
    insert_data_in_userType()
    return render(request, 'home.html', context={})

def productList(request):
    all_product = Products.objects.all()
    print('abcd')
    
    return render(request, 'product/all_products.html', {'all_products' : all_product})


def addBrand(request):
    form = BrandForm(request.POST)
    if form.is_valid():
        form.save()

    context = {
        'form': form
    }
    return render(request, 'product/brand_form.html', context)


def addType(request):
    form = ProductTypeForm(request.POST)
    if form.is_valid():
        form.save()

    context = {
        'form': form
    }
    return render(request, 'product/ptoduct_type_form.html', context)

def searchProduct(request):
    template_name = 'product/all_products.html'
    search_parameter = request.GET.get('product_search_parameter', '')
    search_result = Products.objects.filter(Q(name__icontains=search_parameter) |
                                            Q(description__icontains=search_parameter) |
                                            Q(brand__brand_name__icontains=search_parameter) |
                                            Q(p_type__type_name__icontains=search_parameter)
                                            )
    context = {
        'all_products': search_result
    }
    return render(request, template_name, context )