from django import template
from django.shortcuts import render, redirect
from products.models import Products, ProductType, Brand, SellDetails
from products.forms import *
from shop.models import ShopDetail
from django.core.files.storage import FileSystemStorage
from xlrd import open_workbook
import os
# Create your views here.

def myShopProducts(request):
    template_name = 'shop/my_shop.html'
    this_shop = request.user
    this_shop_store = Products.objects.all()
    context = {
        'all_products': this_shop_store
    }
    return render(request, template_name, context)


def addProductTypeBrand(request):
    template_name = 'shop/product_type_brand.html'
    all_product_type = ProductType.objects.all()
    all_brand = Brand.objects.all()
    product_type_form = ProductTypeForm
    product_brand_form = BrandForm

    context = {
        'product_brand_form': product_brand_form,
        'product_type_form': product_type_form,
        'all_product_type': all_product_type,
        'all_brand': all_brand
    }

    if request.method == 'POST':
        product_type_form = ProductTypeForm(request.POST)
        product_brand_form = BrandForm(request.POST)
        if product_type_form.is_valid():
            product_type_form.save()
        if product_brand_form.is_valid():
            product_brand_form.save()

    return render(request, template_name, context)

def addProduct(request):
    this_shop = request.user
    print(this_shop.id)
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    form = ProductForm
    product_type = ProductType.objects.all()
    brand = Brand.objects.all()
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        p_type = request.POST.get('p_type')
        p_brand = request.POST.get('brand')
        size = request.POST.get('volume_weight')
        buying_pric = request.POST.get('buying_price')
        barcode = barCodeGenerator(p_type, p_brand, size, buying_pric)
        if form.is_valid():
            product = form.save(commit=False)
            product.shop = this_shop_detail
            product.bar_code = barcode
            product.bar_code_image = 'image/bar_codes/'+barcode+'.png'
            product.save()

    context = {
        'form': form,
        'product_types': product_type,
        'brands': brand
    }
    # print(context['form'])
    return render(request, 'product/product_form.html', context)

def editProduct(request, id):
    this_shop = request.user
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    this_product = Products.objects.get(id= id)
    print(this_product.name)
    product_type = ProductType.objects.all()
    brand = Brand.objects.all()
    if request.method == 'POST':
        
        p_type = request.POST.get('type')
        p_brand = request.POST.get('brand')
        p_name = request.POST.get('name')
        p_description = request.POST.get('description')
        if 'image' in request.FILES:
            p_image = request.FILES['image']
        else:
            p_image = None
        p_volume_weight = request.POST.get('volume_weight')
        p_buying_price = request.POST.get('buying_price')
        p_availability = request.POST.get('availability')
        p_selling_price = request.POST.get('selling_price')
        
        size = request.POST.get('volume_weight')
        buying_pric = request.POST.get('buying_price')
        barcode = barCodeGenerator(p_type, p_brand, size, buying_pric)
        # if form.is_valid():
        #     # product = form.save(commit=False)
        #     product.shop = this_shop_detail
        this_product.bar_code = barcode
        this_product.bar_code_image = 'image/bar_codes/'+barcode+'.png'
        this_product.p_type_id = p_type
        this_product.brand_id = p_brand
        this_product.name = p_name
        this_product.description = p_description
        if p_image != None:
            this_product.p_image = p_image
        this_product.volume_weight = p_volume_weight
        this_product.buying_price = p_buying_price
        this_product.available_quantity = p_availability
        this_product.selling_price = p_selling_price

        this_product.save()

        return redirect('shop:my_shop')

    context = {
        'this_product':this_product,
        'product_types': product_type,
        'brands': brand
    }
    return render(request, 'shop/edit_product.html', context)


import barcode
from barcode.writer import ImageWriter
def barCodeGenerator(p_type, brand, size, buying_price):
    code_string = str(p_type)+'-'+str(brand)+'-'+str(size)+'-'+str(buying_price)
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    file_path = 'media/image/bar_codes/'+code_string+'.png'
    file_dir = os.path.join(BASE_DIR, file_path)
    hr = barcode.get_barcode_class('code39')
    
    with open(file_dir, 'wb') as f:
        Hr = hr(code_string, writer=ImageWriter()).write(f)
    
    return code_string

from openpyxl import Workbook
import openpyxl
def addProductFromExcel(request):
    this_shop = request.user
    print(this_shop.id)
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    if request.method == 'GET':
        get_type = ProductType.objects.all()
        return render(request, 'product/add_from_excel.html', {'get_type':get_type})
    if request.method == 'POST' and request.FILES['xlFile']:
        products_type = request.POST.get('type')
        products_type_name = ProductType.objects.get(id=products_type).type_name
        xlFile = request.FILES['xlFile']
        product_images = request.FILES.getlist('images')
        fs = FileSystemStorage()
        file_name = fs.save(xlFile.name, xlFile)
        file_size = fs.size(xlFile.name)
        # print(products_type)
        # print(file_size)
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        file_path = 'media/'+file_name
        file_dir = os.path.join(BASE_DIR, file_path)
        for product_image in product_images:
            fs = FileSystemStorage(location='media/image/products/')
            product_image_name = product_image.name
            product_image_name_str = product_image_name.replace(' ','_')
            image_name = fs.save(product_image_name_str, product_image)
            # print(image_name)
            # product_images_path = 'media/image/products/'+image_name
            # product_images_dir = os.path.join(BASE_DIR, product_images_path)
            
            # print(product_images_dir)
        # print(file_dir)
        items = []

        rows = []
        
        xl_data = openpyxl.load_workbook(file_dir)

        sheet_obj = xl_data.active
        m_row = sheet_obj.max_row
        m_col = sheet_obj.max_column

       
        for row in range(1, m_row+1):
            values = []
            for col in range(1, m_col+1):
                value  = (sheet_obj.cell(row,col).value)
                # print('-----------------')
                # print(value)
                try:
                    value = str(value)
                    # print(value)
                    values.append(value)
                except ValueError:
                    pass
            # item = Arm(*values)
            print(values)
            items.append(values)
        

        for item in items:
            p_name= item[1]+' '+products_type_name
            # p_bar_code= item[2]
            p_description= item[2]
            p_image= 'image/products/'+item[3].replace(' ', '_')
            p_volume= item[4]
            p_buying_price= int(float(item[5]))
            p_available_quantity= int(float(item[6]))
            p_selling_price = (p_buying_price*.2)+p_buying_price
            p_barcode = barCodeGenerator(products_type, 1, p_volume, p_buying_price)
            p_bar_code_image = 'image/bar_codes/'+p_barcode+'.png'

            single_product = Products(name = p_name, description= p_description,
                                    p_image = p_image, volume_weight = p_volume,
                                    buying_price = p_buying_price, available_quantity = p_available_quantity,
                                    bar_code = p_barcode, bar_code_image = p_bar_code_image,
                                    selling_price = p_selling_price, brand_id = 1, p_type_id = products_type,
                                    shop_id = this_shop_detail.id)
                
            single_product.save()

        return redirect('shop:add_excel')

def shopStore(request):
    template_name = 'shop/store.html'
    this_shop = request.user
    print(this_shop)
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    print(this_shop_detail)
    this_shop_store = Products.objects.all().order_by('name', 'p_type', 'brand')
    context = {
        'all_products': this_shop_store
    }
    return render(request, template_name, context)

from django.db.models import Q

def searchMyStore(request):
    template_name = 'shop/store.html'
    this_shop = request.user
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    search_parameter = request.GET.get('product_search_parameter', '')
    search_result = Products.objects.filter(Q(name__icontains=search_parameter) |
                                            Q(description__icontains=search_parameter) |
                                            Q(brand__brand_name__icontains=search_parameter) |
                                            Q(p_type__type_name__icontains=search_parameter) |
                                            Q(bar_code__icontains=search_parameter)
                                            ).order_by('name', 'p_type', 'brand')
    context = {
        'all_products': search_result
    }
    return render(request, template_name, context)


from django.http import HttpResponseRedirect, HttpResponse

def sellProduct(request):
    id = request.GET['id']
    this_shop = request.user
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    quantity = request.GET['quantity']

    print(id)
    print(quantity)

    this_product = Products.objects.get(id = id)
    print(this_product.available_quantity)
    this_product.available_quantity = int(this_product.available_quantity) - int(quantity)
    this_product.save()
    shop_id = this_shop_detail.id
    this_product_id = this_product.id
    quantity = int(quantity)
    buying_price = int(this_product.buying_price)
    selling_price = buying_price + buying_price*0.15
    profit = selling_price - buying_price
    insertSell(shop_id, this_product_id, quantity, selling_price, profit)

    this_user = request.user

    return HttpResponse(id)

def insertSell(shop, p_id, quantity, selling_price, profit):
    sell_details = SellDetails(shop_id = shop, product_id = p_id, quantity = quantity, selling_price = selling_price, profit=profit)
    print('product sold')
    sell_details.save()

def allSell(request):
    template_name = 'shop/accounts.html'
    this_shop = request.user
    print(this_shop)
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    print(this_shop_detail)

    all_sell = SellDetails.objects.filter(shop = this_shop_detail)
    
    print(all_sell)
    context = {
        "all_sell" : all_sell
    }

    return render(request, template_name, context)

from datetime import datetime, date, time

def filterSell(request):
    template_name = 'shop/accounts.html'
    this_shop = request.user
    print(this_shop)
    this_shop_detail = ShopDetail.objects.get(shop_id = this_shop.id)
    print(this_shop_detail)


    if request.GET.get('from_date')!='':                                   ### if date(from) is submitted into search form
        from_date= request.GET.get('from_date')
    else:
        from_date="2021-01-01"                                             ### if not using default value

    if request.GET.get('to_date')!='':                                   ### if date(to) is submitted into search form
        to_date= request.GET.get('to_date')
    else:
        to_date=date.today().strftime("%Y-%m-%d")                          ### if not, using todays date as default value

    try:
        from_date = from_date + ' 00:00:00.000000'                       ### adding time with date, for accuracy perpose
        to_date = to_date + ' 23:59:59.999999'
    except Exception as e:
        print(e)


    # from_date = request.GET.get('from_date', '')
    # to_date = request.GET.get('to_date', '')
    month = request.GET.get('month', '')
    year = request.GET.get('year', '')

    if month != '' and year == '':
        this_day = date.today()
        this_year = str(this_day).split('-')[0]
        
        month_year = this_year +'-'+ month
        print(month_year)

    elif month != '' and year != '':
        this_year = year
        
        month_year = this_year +'-'+ month
        print(month_year)

    elif month == '' and year != '':
        this_year = year

        month_year = this_year +'-'
        print(month_year) 

    else:
        month_year = ''

    product = request.GET.get('product', '')

    search_result = SellDetails.objects.filter(Q(created_at__gte=from_date)&Q(created_at__lte=to_date) ,
                                                Q(created_at__icontains = month_year) ,
                                                Q(product__name__icontains = product) |
                                                Q(product__bar_code__icontains = product)
                                            ).order_by('-created_at')
    context = {
        'all_sell': search_result
    }

    return render(request, template_name, context)
