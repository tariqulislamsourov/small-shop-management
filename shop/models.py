from django.db import models
from account.models import User, UserType
# Create your models here.


class ShopDetail(models.Model):
    shop = models.ForeignKey(User, on_delete=models.CASCADE)
    shop_owner_name = models.CharField(max_length=25, blank=True, default='')
    shop_owner_phone = models.CharField(max_length=20, blank=True, default='')
    shop_type = models.CharField(max_length= 15, blank=True, default='')
    shop_address = models.CharField(max_length= 50, blank=True, default='')
    updated_at = models.DateTimeField(auto_now=True)

    def __int__(self):
        return self.id