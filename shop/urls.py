from os import name
from django.contrib import admin
from django.urls import path, re_path

from django.conf.urls import url
from shop.views import *

app_name = 'shop'

urlpatterns = [
    path('my-shop/', myShopProducts, name='my_shop'),
    path('add-product/', addProduct, name='add_product'),
    path('edit-product/<int:id>', editProduct, name='edit_product'),
    path('add-type-brand/', addProductTypeBrand, name='add_type_brand'),
    # path('add-type', addType, name='addType'),
    path('add-excel/', addProductFromExcel, name = 'add_excel'),
    path('store/', shopStore, name = 'store'),
    path('search-self-store/', searchMyStore, name= 'search_self_store'),
    path('testbr/', barCodeGenerator),

    path('sell/sellProduct/', sellProduct, name='sell_product'),
    path('all-sell/', allSell, name="all_sell"),

    path('filter-sell/', filterSell, name= "filter_sell")
]