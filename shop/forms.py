from django import forms

from account.models import User, UserType
from shop.models import ShopDetail

import re

from django.core.exceptions import ValidationError


class ShopCreateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'email', 'password', 'phone', 'is_active']

        error_messages = {
             'first_name': {
                'required': 'Name is required',
            },
            'email': {
                'required': 'email is required.'
            },
            'password': {
                'required': 'Password is required.'
            },
            'phone': {
                'required': 'Phone is important.'
            },
        }
    def __init__(self, *args, **kwargs):
        super(ShopCreateForm, self).__init__(*args, **kwargs)
       
        self.fields['phone'].required = True
        self.fields['first_name'].required = True
        self.fields['password'].required = True

    def clean_phone(self, *args, **kwargs):
        phone = self.cleaned_data.get('phone')
        if len(phone) < 11:
            raise forms.ValidationError("phone number is too short")
        elif re.search('[a-zA-Z]', phone):
            raise forms.ValidationError("Invalid phone number")
        else:
            return phone

class ShopDetailForm(forms.ModelForm):
    class Meta:
        model = ShopDetail
        fields = ['shop_owner_name','shop_owner_phone', 'shop_type', 'shop_address']