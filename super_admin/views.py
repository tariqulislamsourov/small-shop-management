from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from django.contrib import messages, auth
from django.db.models import Q
from account.models import *
from shop.models import ShopDetail
from shop.forms import ShopCreateForm, ShopDetailForm
from account.models import *
from account.forms import *
from products.models import *
from products.forms import *

def superAdminDashboard(request):
    template_name = 'super_admin/dashboard.html'

    return render(request, template_name, {})

def addShop(request):
    template_name = 'super_admin/add_shop.html'
    shop_form = ShopCreateForm
    shop_detail_form = ShopDetailForm
    if request.method == 'POST':
        shop_form = ShopCreateForm(request.POST)
        shop_detail_form = ShopDetailForm(request.POST)
        print(shop_form)
        if shop_form.is_valid():
            print('form is valid')
            shop = shop_form.save(commit=False)
            password = shop_form.cleaned_data.get('password')
            shop.set_password(password)
            shop.is_staff = False
            shop.is_superuser = False
            shop.user_type = UserType.objects.get(user_type='shop')         ## defining user_type as doctor for user table
            shop.is_active = True
            print("shop create form saved")
            shop.save()
        if shop_detail_form.is_valid:
            shop_details = shop_detail_form.save(commit=False)
            this_shop_email = shop_form.cleaned_data.get('email')
            this_shop = User.objects.get(email = this_shop_email)
            shop_details.shop = this_shop
            shop_details.save()


    context = {
        'shop_form':shop_form,
        'shop_detail_form':shop_detail_form
    }

    return render(request, template_name, context)

def allShopList(request):
    template_name = 'super_admin/all_shop.html'
    all_shop = ShopDetail.objects.all()
    print(all_shop)
    context = {
        'all_shop':all_shop
    }
    return render(request, template_name, context)

def addProductTypeBrand(request):
    template_name = 'super_admin/product_type_brand.html'
    all_product_type = ProductType.objects.all()
    all_brand = Brand.objects.all()
    product_type_form = ProductTypeForm
    product_brand_form = BrandForm

    context = {
        'product_brand_form': product_brand_form,
        'product_type_form': product_type_form,
        'all_product_type': all_product_type,
        'all_brand': all_brand
    }

    if request.method == 'POST':
        product_type_form = ProductTypeForm(request.POST)
        product_brand_form = BrandForm(request.POST)
        if product_type_form.is_valid():
            product_type_form.save()
        if product_brand_form.is_valid():
            product_brand_form.save()

    return render(request, template_name, context)

def deleteProductType(request, id=None):
    this_product_type = ProductType.objects.get(id = id)
    this_product_type.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def deleteBrand(request, id=None):
    this_brand = Brand.objects.get(id = id)
    this_brand.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))