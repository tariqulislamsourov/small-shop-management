from django.contrib import admin

# Register your models here.
from account.models import UserType, User

admin.site.register(User)
admin.site.register(UserType)