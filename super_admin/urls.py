from django.contrib import admin
from django.urls import path,re_path, include

from django.conf.urls import url
from super_admin.views import *
app_name = 'super_admin'

urlpatterns = [
    
    path('dashboard/', superAdminDashboard, name='dashboard'),
    path('add-shop/', addShop, name='add_shop'),
    path('all-shop/', allShopList, name='all_shop'),
    path('add-type-brand/', addProductTypeBrand, name='add_type_brand'),
    path('delete-type/<int:id>', deleteProductType, name='delete_product_type'),
    path('delete-brand/<int:id>', deleteBrand, name='delete_brand'),
    
]