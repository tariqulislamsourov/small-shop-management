from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, FormView, RedirectView
from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from django.core.paginator import Paginator
from django.contrib import messages, auth
from django.db.models import Q
from account.models import *

from account.models import *
from account.forms import *

from datetime import datetime, date, time, timezone
# Create your views here.

class LoginView(FormView):                                           ##----------- View function for login. Extending Django FormView class
    super_admin_success_url= '/super-admin/dashboard/'               ##----------- success url if logged in as user_type == admin
    shop_success_url= '/shop/my-shop/'
    # doctor_success_url= '/doctor/dashboard/'
    # manager_success_url= '/manager/dashboard/'
    # general_staff_success_url= '/staff/dashboard/'

    general_success_url= '/'                                         ##----------- if user type is undefined


    form_class = LoginForm
    template_name = "account/login.html"

    extra_context = {
        'title': 'Login'
    }
    # user_role = UserType.objects.get(user_type='admin')

    def dispatch(self, request, *args, **kwargs):                     ##----------- Track and redirect as the requisting user is authenticated 
        if self.request.user.is_authenticated:
            return redirect(self.get_success_url())                   ##----------- if authenticated redirect to success url as user_type
        return super().dispatch(self.request, *args, **kwargs)        ##----------- if not authenticated redirect to login page

    def get_success_url(self):                                        ##----------- finds the correct success url as user_type
        if 'next' in self.request.GET and self.request.GET['next'] != '':
            return self.request.GET['next'] 

        else:
            if self.request.user.user_type == UserType.objects.get(user_type='admin'):                ##----------- if admid return the admin dashboard
                print(self.request.user.user_type)
                return self.super_admin_success_url
            if self.request.user.user_type == UserType.objects.get(user_type='shop'):                ##----------- if client return the client dashboard
                print(self.request.user.user_type)
                return self.shop_success_url
            # if self.request.user.user_type == UserType.objects.get(user_type='doctor'):                ##----------- if doctor return the doctor dashboard
            #     print(self.request.user.user_type)
            #     return self.doctor_success_url
            # if self.request.user.user_type == UserType.objects.get(user_type='manager'):                ##----------- if manger return the manager dashboard
            #     print(self.request.user.user_type)
            #     return self.manager_success_url
            # if self.request.user.user_type == UserType.objects.get(user_type='general_staff'):                ##----------- if general_staff return the general_staff dashboard
            #     print(self.request.user.user_type)
            #     return self.general_staff_success_url
            else:
                print(self.request.user.user_type)
                return self.general_success_url


    def get_form_class(self):                                         ##----------- get the form of this class
        print(self.form_class(data=self.request.POST))
        return self.form_class

    def form_valid(self, form):                                       ##----------- if submited form is valid get the user and call success_url
        print("valid Form")
        auth.login(self.request, form.get_user())

        # write_log(self.request, self.request.user, "Logged In", CHANGE)

        return redirect(self.get_success_url())

    def form_invalid(self, form):
        print("invalid form")
        return self.render_to_response(self.get_context_data(form=form)) ##------ if submited form is invalid load the form again

    # return render(request, 'login.html', {})

class LogoutView(RedirectView):                                          ##------ Control if user is logging out

    url = '/account/login/'                                                      ##------ url after logout

    def get(self, request, *args, **kwargs):

        # write_log(self.request, self.request.user, "Logged Out", CHANGE)

        auth.logout(request)
        print('Logged out')
        print(request.user)
        messages.success(request, "You are logged out")
        return super(LogoutView, self).get(request, *args, **kwargs)



def passwordReset(request):                                              ##-------- This function is to reset passwod
    if request.method == 'POST':
        email = request.POST.get('email')
        pass1= request.POST.get('password1')
        pass2= request.POST.get('password2')
        # print(email)
        try: 
            email= User.objects.get(email=email)
            if email:                                                    ##-------- Checking if the given email is into user table (registered)
                if pass1 == pass2:                                       ##-------- confirming the new password
                    email.set_password(pass1)                            ##-------- set new password
                    email.save()                                         ##-------- save the password
            return redirect('login_page')                                ## ------- after successful reset redirect to login page
        except Exception as e:
            print(e)
            context = {'message':"You are not registered"}
            return render(request, 'reset_password.html', context)      ##--------- if given email is wrong render with message
    return render(request, 'reset_password.html', {})


# ------------ class to create new meditech admin-------------------------
class SuperAdminRegistrationView(CreateView):
    model = User
    form_class = AdminRegistrationForm
    template_name = 'account/super_admin_register.html'
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):                       ##----------- Track and redirect as the requisting user is authenticated
        if self.request.user.is_authenticated:
            return redirect('home')
        return super().dispatch(self.request, *args, **kwargs)

    def post(self, request, *args, **kwargs):                           ##----------- if request is post
        form = self.form_class(data=request.POST)                       ##----------- assigning form with post data
        # print(form)
        try:
            if form.is_valid():
                user = form.save(commit=False)                          ##----------- commit=false gets and model and make the form(with data)
                                                                        ##----------- editable and change the data into form
                password = form.cleaned_data.get('password1')
                
                user.set_password(password)
                user.is_superuser = True
                user.is_staff = True
                
                user.save()
                return redirect('home')        ##---------- redirect to admin dashboard if successful
            else:
                return render(request, 'account/super_admin_register.html', {'form':form})
        except Exception as e:
            print(e)
    #return render(request, 'meditech_admin/meditech_admin_register.html')


def insert_data_in_userType():

    count = UserType.objects.all().count()

    if count == 0:
        obj = UserType()
        obj.id = 1
        obj.user_type = 'admin'
        obj.save()

        obj = UserType()
        obj.id = 1
        obj.user_type = 'shop'
        obj.save()

        obj = UserType()
        obj.id = 1
        obj.user_type = 'customer'
        obj.save()